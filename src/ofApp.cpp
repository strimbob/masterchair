#include "ofApp.h"

//--------------------------------------------------------------
void ofApp::setup(){
	
	// on OSX: if you want to use ofSoundPlayer together with ofSoundStream you need to synchronize buffersizes.
	// use ofFmodSetBuffersize(bufferSize) to set the buffersize in fmodx prior to loading a file.
	

    cout << "starting Up start first " << endl;
    playingOne.load("1085.mp3");
    message = "";
    string path = "sounds/";
    ofDirectory dir(path);
    dir.allowExt("mp3");
    dir.listDir();
    
    //go through and print out all the paths
    for(int i = 0; i < dir.size(); i++){
        vector  <string> paths = ofSplitString( dir.getPath(i), "/");
        audioFiles tmp;
        tmp.load(paths[1]);
        allTheFiles.push_back(tmp);
        cout << "loading" << " " << paths[1] << endl;
    }
    serial.setup(0, 9600);
    read();
    ofAddListener(NEW_MESSAGE,this,&ofApp::onNewMessage);


}

//--------------------------------------------------------------
void ofApp::writeString(string message){
    unsigned char* chars = (unsigned char*) message.c_str(); // cast from string to unsigned char*
    int length = message.length();
    serial.writeBytes(chars, length);
}



//--------------------------------------------------------------
void ofApp::update(){
 read();



}

//--------------------------------------------------------------
void ofApp::draw(){


}


//--------------------------------------------------------------
void ofApp::onNewMessage(string & message){
   
    vector<string> input = ofSplitString(message, ",");
    if(input.size() >= 1){
        if     ("P" == input.at(0)) play();
        else if("p" == input.at(0)) pause();
        else if("s" == input.at(0)) stop();
        else if("r" == input.at(0)) rewind();
        else if("t" == input.at(0)) track(input.at(1));
        else if("S" == input.at(0)) status();
        else if("W" == input.at(0)) getAllPaths();
    }
    
    cout << message << endl;
}

//--------------------------------------------------------------
void ofApp::getAllPaths(){
    
    cout << "getting all paths " << endl;
    string sendMessage = "@>"+ofToString(allTheFiles.size());
    for(int q = 0;q<allTheFiles.size();q++){
        sendMessage += ">";
        sendMessage += allTheFiles[q].path;
        }
    sendMessage += "?";
    writeString(sendMessage);
    
}

//--------------------------------------------------------------
void ofApp::play(){
    cout << "play" << endl;
    playingOne.play();
}
//--------------------------------------------------------------
void ofApp::pause(){
    
    cout << "pause" << endl;
    playingOne.pause();
    
}
//--------------------------------------------------------------
void ofApp::stop(){
    cout << "stop" << endl;
    playingOne.stop();
    
}
//--------------------------------------------------------------
void ofApp::rewind(){
    cout << "rewind" << endl;
    playingOne.rewind();
    
}
//--------------------------------------------------------------
void ofApp::track(string filname){
     cout << playingOne.path << "changin tht track" << endl;
    bool success = false;
    for(int q = 0;q<allTheFiles.size();q++){
        if(allTheFiles[q].path == filname){
            playingOne.stop();
            playingOne = allTheFiles[q];
            success = true;
        }
    }
    if(!success){
        cout << " error not setting track from file name" << endl;
    }
    
    cout << playingOne.path << endl;
}

//--------------------------------------------------------------
void ofApp::status(){
    cout << "sendinfo" << endl;
//    0 = getinfo
//    1 = path
//    2 = isplaying
//    3 = tracksAvailable
//    4 = duration
    string sendMessageStatus = "$>"+playingOne.path+
                                ">"+ofToString(playingOne.isPlaying())+
                                ">"+ofToString(allTheFiles.size())+
                                ">"+ofToString(playingOne.getLengthTrack()) +
                                ">?";
    
    string sendMessage = "$>sounds/1.mp3>1>6>4.00>?";
    writeString(sendMessage);
}

//--------------------------------------------------------------
void ofApp::read(){
    
    // if we've got new bytes
    if (serial.available() > 0){
        // we will keep reading until nothing is left
        while (serial.available() > 0)
        {
            // we'll put the incoming bytes into bytesReturned
            serial.readBytes(bytesReturned, NUM_BYTES);
            
            // if we find the splitter we put all the buffered messages
            //   in the final message, stop listening for more data and
            //   notify a possible listener
            // else we just keep filling the buffer with incoming bytes.
            if (*bytesReturned == '\n')
            {
                message = messageBuffer;
                messageBuffer = "";
                // cout << "onNewMessage, message: " << message << "\n";
              //  ofRemoveListener(ofEvents().update, this, &ofxSimpleSerial::update);
                ofNotifyEvent(NEW_MESSAGE, message, this);
                
                break;
            }
            else
            {
                if (*bytesReturned != '\r')
                    messageBuffer += *bytesReturned;
            }
            //cout << "  messageBuffer: " << messageBuffer << "\n";
        }
        
        // clear the message buffer
        memset(bytesReturned, 0, NUM_BYTES);
    }
}




//--------------------------------------------------------------
void ofApp::keyPressed  (int key){

    if(key == 'q') writeString("P?"); // play
    if(key == 'a') writeString("p?"); // pause
    if(key == 'z') writeString("s?"); // stop
    if(key == 'w') writeString("r?"); // rewind
    if(key == 's') writeString("t?"); // SelTrack
    if(key == 'x') writeString("W?"); // status
    if(key == 'e') writeString("S?"); // getAllPaths


}

//--------------------------------------------------------------
void ofApp::keyReleased(int key){

}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y ){

}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button){
}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseEntered(int x, int y){

}

//--------------------------------------------------------------
void ofApp::mouseExited(int x, int y){

}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h){

}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg){

}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo){

}
