#pragma once

#include "ofMain.h"

#define NUM_BYTES 1
#include "ofxSocketIO.h"
#include "ofxSocketIOData.h"

class audioFiles {
public:
    static ofEvent<string> fadeOutEvent;
    string fileName;
    string path;
    ofSoundPlayer audioTrack;
    float durationMS;
    bool trackPause = false;
    float fadeOut =  300;
    bool sendAuioBang = true;
    
    void load(string _path){
        sendAuioBang = true;
        fileName = _path;
        path = "sounds/"+fileName;
        audioTrack.load(path, true); //stream mode
        audioTrack.play();
        audioTrack.setPosition(0.5f);
        durationMS  = 2 * audioTrack.getPositionMS(); // wink
        audioTrack.stop();
        audioTrack.setPosition(0.0f);
        cout <<  durationMS <<  " " << fileName << " loaded " <<  endl;
    }
    
    string getLenghtMS(){
        return ofToString(durationMS);
    }
    
    void pause(){
        sendAuioBang = true;
        trackPause = !trackPause;
        audioTrack.setPaused(trackPause);
        
    }
    
    void play(){
        sendAuioBang = true;
        audioTrack.play();
    }
    
    void stop(){
        sendAuioBang = true;
        audioTrack.stop();
    }
    void rewind(){
        sendAuioBang = true;
        audioTrack.setPosition(0);
    }
    
    string isPlaying(){
        if(audioTrack.isPlaying()){
            return "playing";
        }else{
            return "stopped";
        }
    }
    
    string getPlayingTrack(){
        return fileName;
    }
    
    string getLengthTrack(){
        return ofToString(durationMS);
    }
    
    void update(){
        if(audioTrack.isPlaying()){
            if(audioTrack.getPositionMS() > durationMS - fadeOut){
                if( sendAuioBang){
                    string stopping = "stopping in " + ofToString(fadeOut);
                    ofNotifyEvent(audioFiles::fadeOutEvent,stopping);
                    sendAuioBang = false;
                }
            }
        }else{
            sendAuioBang = true;
        }
        
    }
    
    
};



class ofApp : public ofBaseApp{

	public:
		void setup();
		void update();
		void draw();

		void keyPressed  (int key);
		void keyReleased(int key);
		void mouseMoved(int x, int y );
		void mouseDragged(int x, int y, int button);
		void mousePressed(int x, int y, int button);
		void mouseReleased(int x, int y, int button);
		void mouseEntered(int x, int y);
		void mouseExited(int x, int y);
		void windowResized(int w, int h);
		void dragEvent(ofDragInfo dragInfo);
		void gotMessage(ofMessage msg);
		
		ofSoundPlayer  beats;
		ofSoundPlayer  synth;
		ofSoundPlayer  vocals;

		ofTrueTypeFont	font;
		float 			synthPosition;



        void getAllPaths();
    ofEvent<string> NEW_MESSAGE;
        ofSerial serial;
   void writeString(string message);
    unsigned char	bytesReturned[NUM_BYTES];
    void onNewMessage(string & message);
  void  read();
    string			messageBuffer;
    string message;
    void status();
    
    
    void	play();
    void	pause();
    void	stop();
    void	rewind();
    void    track(string input);
    audioFiles playingOne;
    vector < audioFiles > allTheFiles;



		
};

